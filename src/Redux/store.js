import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import axiosReducer from './axios/axiosReducer'

const store = createStore(axiosReducer, applyMiddleware(thunk))

export default store;