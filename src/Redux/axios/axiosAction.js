import { FETCH_PRODUCT_REQUEST, FETCH_PRODUCT_SUCESS, FETCH_PRODUCT_FAILURE } from './axiosTypes'

export const fetchProductRequest = () => {
  console.log('step 1')
  return {
    type: FETCH_PRODUCT_REQUEST
  }
}

export const fetchProductSucess = products => {
  console.log(products)
  return {
    type: FETCH_PRODUCT_SUCESS,
    payload: products
  }
}

export const fetchProductFailure = error => {
  return {
    type: FETCH_PRODUCT_FAILURE,
    payload: error
  }
}