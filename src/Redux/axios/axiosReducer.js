import { FETCH_PRODUCT_REQUEST, FETCH_PRODUCT_SUCESS, FETCH_PRODUCT_FAILURE } from './axiosTypes'

const initialState = {
  loading: false,
  product: [],
  error: '',
  message: 'hi'
}

const fetchReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_REQUEST:
      console.log('step 2')

      return {
        ...state,
        loading: true
      }
    case FETCH_PRODUCT_SUCESS:
      return {
        ...state,
        loading: false,
        product: action.payload,
        error: ''
      }
    case FETCH_PRODUCT_FAILURE:
      return {
        ...state,
        loading: false,
        product: [],
        error: action.payload
      }

    default: return state
  }
}

export default fetchReducer