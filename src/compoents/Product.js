import axios from 'axios';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchProductRequest, fetchProductSucess, fetchProductFailure } from '../Redux'

const fetchProduct = (props) => {
  return function () {
    props.fetchProductRequest()
    axios.get('https://fakestoreapi.com/products')
      .then(res => {
        const product = res.data
        props.fetchProductSucess(product)
      })
      .catch(error => {
        props.fetchProductFailure(error.message)
      })
  }
}
function Product(props) {
  console.log(props)

  return <div>
    <h2>Hello world{props.message}</h2>
    <button onClick={fetchProduct(props)}>Buy Cake</button>
  </div>;
}

const mapStateToProps = (state) => {
  return {
    message: state.message,
    loading: state.loading,
    product: state.product,
    error: state.error,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProductRequest: () => dispatch(fetchProductRequest()),
    fetchProductSucess: (product) => dispatch(fetchProductSucess(product)),
    fetchProductFailure: (error) => dispatch(fetchProductFailure(error))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Product);

