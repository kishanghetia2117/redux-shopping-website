import './App.css';
import { Provider } from 'react-redux';
import store from './Redux/store'
import Product from './compoents/Product';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Product />
      </div>
    </Provider>
  );
}

export default App;
